variable "avail_zone" {}
variable "myip" {}
variable "instance_type" {}
variable "public_key" {}
variable "image_name" {}


variable "stag_access_key" {}
variable "stag_secret_key" {}

variable "prod_access_key" {}
variable "prod_secret_key" {}

variable "vpc_cidr_block_stag" {}
variable "subnet_cidr_block_stag" {}

variable "vpc_cidr_block_prod" {}
variable "subnet_cidr_block_prod" {}

variable "env_prefix_stag" {}
variable "env_prefix_prod" {}
