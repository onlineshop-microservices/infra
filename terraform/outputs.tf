output "ec2_public_ip_stag" {
  value = module.myapp-webserver-stag.instance.public_ip
}
output "ec2_public_ip_prod" {
  value = module.myapp-webserver-prod.instance.public_ip
}
