terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  cloud {
    organization = "prozip"

    workspaces {
      name = "onlineshop"
    }
  }
}

provider "aws" {
  alias      = "stag"
  region     = "ap-southeast-1"
  access_key = var.stag_access_key
  secret_key = var.stag_secret_key
}

provider "aws" {
  alias      = "prod"
  region     = "ap-southeast-2"
  access_key = var.prod_access_key
  secret_key = var.prod_secret_key
}

# create stag account resource
resource "aws_vpc" "myapp-vpc-stag" {
  cidr_block           = var.vpc_cidr_block_stag
  enable_dns_hostnames = true
  tags = {
    "Name" = "${var.env_prefix_stag}-vpc",
  }
  provider = aws.stag
}

module "myapp-subnet-stag" {
  source                 = "./modules/subnet"
  subnet_cidr_block      = var.subnet_cidr_block_stag
  avail_zone             = var.avail_zone
  env_prefix             = var.env_prefix_stag
  vpc_id                 = aws_vpc.myapp-vpc-stag.id
  default_route_table_id = aws_vpc.myapp-vpc-stag.default_route_table_id
  providers = {
    aws = aws.stag
  }
}

module "myapp-webserver-stag" {
  source               = "./modules/webserver"
  vpc_id               = aws_vpc.myapp-vpc-stag.id
  myip                 = var.myip
  env_prefix           = var.env_prefix_stag
  image_name           = var.image_name
  public_key           = var.public_key
  instance_type        = var.instance_type
  subnet_id            = module.myapp-subnet-stag.subnet.id
  avail_zone           = var.avail_zone
  providers = {
    aws = aws.stag
  }
}

# create production account resource
resource "aws_vpc" "myapp-vpc-prod" {
  cidr_block           = var.vpc_cidr_block_prod
  enable_dns_hostnames = true
  tags = {
    "Name" = "${var.env_prefix_prod}-vpc",
  }
  provider = aws.prod
}

module "myapp-subnet-prod" {
  source                 = "./modules/subnet"
  subnet_cidr_block      = var.subnet_cidr_block_prod
  avail_zone             = "ap-southeast-2a"
  env_prefix             = var.env_prefix_prod
  vpc_id                 = aws_vpc.myapp-vpc-prod.id
  default_route_table_id = aws_vpc.myapp-vpc-prod.default_route_table_id
  providers = {
    aws = aws.prod
  }
}

module "myapp-webserver-prod" {
  source               = "./modules/webserver"
  vpc_id               = aws_vpc.myapp-vpc-prod.id
  myip                 = var.myip
  env_prefix           = var.env_prefix_prod
  image_name           = var.image_name
  public_key           = var.public_key
  instance_type        = var.instance_type
  subnet_id            = module.myapp-subnet-prod.subnet.id
  avail_zone           = "ap-southeast-2a"
  providers = {
    aws = aws.prod
  }
}

# # run ansible script
# resource "null_resource" "configure_server_stag" {
#   triggers = {
#     trigger = module.myapp-webserver-stag.instance.public_ip
#   }
#   provisioner "local-exec" {
#     working_dir = "../ansible"
#     command     = "ansible-playbook -i ${module.myapp-webserver-stag.instance.public_ip}, --private-key ${var.private_key_location} -e 'runner_name=test reg_token=GR1348941V43GMgt8yxhAUALFoXG-' -u ubuntu deploy-gitlab-runner.yaml"
#   }
# }
# resource "null_resource" "configure_server_prod" {
#   triggers = {
#     trigger = module.myapp-webserver-prod.instance.public_ip
#   }
#   provisioner "local-exec" {
#     working_dir = "../ansible"
#     command     = "ansible-playbook -i ${module.myapp-webserver-prod.instance.public_ip}, --private-key ${var.private_key_location} -e 'runner_name=prod reg_token=GR1348941V43GMgt8yxhAUALFoXG-' -u ubuntu deploy-gitlab-runner.yaml"

#   }
# }


